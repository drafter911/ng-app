export default class PhonesController {
    constructor($http) {
        this.search = '';
        let that = this;
        $http({
            method: 'GET',
            url: '../../data/phones.json'
        }).then((response) => {
            that.phones = response.data;
        }, (response) => {
            console.error('something wrong');
        })
    }
}

PhonesController.$inject = ['$http'];
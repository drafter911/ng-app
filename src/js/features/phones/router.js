let routes = ($stateProvider) => {
    $stateProvider
    .state('phones', {
            url: '/phones',
            template: require('./phones.html'),
            controller: 'PhonesController',
            controllerAs: 'phones'
        });
};

routes.$inject = ['$stateProvider'];

export default routes;
import './phones.scss';
import angular from 'angular';
import uirouter from 'angular-ui-router';
import PhonesController from './PhonesController'
import routes from './router';

export default angular.module('app.phones', [
    uirouter
])
.config(routes)
.controller('PhonesController', PhonesController)
.name;
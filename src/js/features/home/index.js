import angular from 'angular';
import uirouter from 'angular-ui-router';
import routes from './router';
import HomeController from './homeController';

export default angular.module('app.home',
    [
        uirouter
    ])
    .config(routes)
    .controller('HomeController', HomeController)
    .name;
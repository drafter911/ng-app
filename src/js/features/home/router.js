let routes = ($stateProvider) => {
    $stateProvider
        .state('home', {
            url: '/',
            template: require('./home.html'),
            controller: 'HomeController',
            controllerAs: 'home'
        });
};

routes.$inject = ['$stateProvider'];

export default routes;
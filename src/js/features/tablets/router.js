let routes = ($stateProvider) => {
    $stateProvider
    .state('tablets', {
            url: '/tablets',
            template: require('./tablets.html'),
            controller: 'TabletsController',
            controllerAs: 'tablets'
        });
};

routes.$inject = ['$stateProvider'];

export default routes;
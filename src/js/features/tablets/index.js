import angular from 'angular';
import uirouter from 'angular-ui-router';
import TabletsController from './TabletsController';
import routes from './router';

export default angular.module('app.tablets', [
    uirouter
])
.config(routes)
.controller('TabletsController', TabletsController)
.name;
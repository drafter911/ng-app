import angular from 'angular';

let routing = ($urlRouterProvider, $locationProvider) => {
    $locationProvider.html5Mode(false);
    //$locationProvider.html5Mode({
    //    enabled: true,
    //    requireBase: false
    //});
    $urlRouterProvider.otherwise('/');
};

routing.$inject = ['$urlRouterProvider', '$locationProvider'];

export default routing;
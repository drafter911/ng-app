import angular from 'angular';
import './footer.scss';

let Footer = () => {
    return {
        restrict: 'EA',
        template: require('./index.html')
    }
};

export default  angular.module('directives.footer', [])
    .directive('footer', Footer)
    .name;
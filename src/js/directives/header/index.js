import angular from 'angular';
import './header.scss';

let Header = () => {
    return {
        restrict: 'EA',
        template: require('./index.html')
    }
};

export default angular.module('directives.header', [])
    .directive('header', Header)
    .name;
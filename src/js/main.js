import './main.scss';
import angular from 'angular';
import uirouter from 'angular-ui-router';
import routing from './config/app.config';
import Footer from './directives/footer';
import Header from './directives/header';
import home from './features/home';
import phones from './features/phones';
import tablets from './features/tablets';

let app = angular.module('app',
    [
        uirouter,
        Footer,
        Header,
        home,
        phones,
        tablets
    ]);

app.config(routing);

//export default app;